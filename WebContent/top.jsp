<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>掲示板システム</title>
    </head>
    <body>
        <div class="main-contents">
            <div class="header">
             <h1>ユーザー登録情報一覧</h1>
                <a href="signup">新規登録</a>
            </div>
            </div>


    <div class="userinfo">
    <c:forEach items="${userinfo}" var="users">
          <table>
                <c:out value="${users.login_id} | " />
                <c:out value="${users.name} | " />
                <c:out value="${users.branches_name} | " />
                <c:out value="${users.divisions_name} | " />
                <a href="settings?id=${users.id}">編集</a>
           </table>
    </c:forEach>
  </div>


            <div class="copyright"> Copyright(c)Numayama</div>
    </body>
</html>