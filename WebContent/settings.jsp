<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ユーザー情報編集</title>
    </head>
    <body>
        <div class="main-contents">

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="messages">
                            <li><c:out value="${messages}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>

            <form action="settings" method="post"><br />

            	<input type="hidden" value="${edit.id}" name="id" />
                <label for="login_id">ログインID</label>
                <input name="login_id" value="${edit.login_id}" id="login_id" /> <br />

                <label for="password">変更用パスワード</label>
                <input name="password" type="password" id="password"/> <br />

                <label for="password2">確認用パスワード</label>
                <input name="password2" type="password" id="password2" /> <br />

                <label for="name">名前</label>
                <input name="name" value="${edit.name}" id="name" /> <br />

                <label for="branch">支店</label>
				<select name="branch" size="1" id="branch" >
               	<c:forEach items="${branch}" var="bra">
		        <c:if test="${bra.id == edit.store}">
				<option value="${bra.id}" selected> ${bra.branches_name}</option>
				</c:if>
				<c:if test="${bra.id != edit.store}">
				<option value="${bra.id}"><c:out value="${bra.branches_name}"/></option>
				</c:if>
                </c:forEach>
                </select>
                <br />


                <label for="division">部署・役職</label>
                <select name="division" size="1" id="division">
                <c:forEach items="${division}" var="di">
                <c:if test="${di.id == edit.department}">
				<option value="${di.id}" selected> ${di.divisions_name}</option>
				</c:if>
				<c:if test="${di.id != edit.department}">
				<option value="${di.id}"><c:out value="${di.divisions_name}"/></option>
				</c:if>
                </c:forEach>
                </select>
                <br />

                <input type="submit" value="保存" /> <br />
                <a href="./">戻る</a>
            </form>
            <div class="copyright"> Copyright(c)Numayama</div>
        </div>
    </body>
</html>