<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>新規登録</title>
    </head>
    <body>
        <div class="main-contents">

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
            </div>

            <div class="form">

            <form action="signup" method="post"><br />
                <label for="login_id">ログインID</label>
                <input name="login_id" id="login_id" /> <br />

                <label for="password">パスワード</label>
                <input name="password" type="password" id="password" /> <br />

                <label for="password2">パスワード確認用</label>
                	<input name="password2" type="password" id="password2" /> <br />

                <label for="name">名前</label>
                <input name="name" id="name" /><br />

                <label for="branch">支店</label>
                <select name="branch" size=1 id="branch">
               		<c:forEach items="${branch}" var="bra">
	                <option value="${bra.id}">${bra.branches_name}</option>
                    </c:forEach>
                </select><br />

                <label for="division">部署・役職</label>
                <select name="division" size=1 id="division">
                	<c:forEach items="${division}" var="di">
	                <option value="${di.id}">${di.divisions_name}</option>
                    </c:forEach>
                </select><br />

                <br /> <input type="submit" value="登録" /> <br /> <a href="./">戻る</a>
            </form>
            <div class="copyright">Copyright(c)Numayama</div>
        </div>
    </body>
</html>