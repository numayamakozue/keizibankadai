package kadai5.dao;

import static kadai5.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.commons.lang.StringUtils;

import kadai5.beans.User;
import kadai5.exception.NoRowsUpdatedRuntimeException;
import kadai5.exception.SQLRuntimeException;

public class UserDao {

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("login_id, ");
            sql.append("name, ");
            sql.append("password, ");
            sql.append("store, ");
            sql.append("department, ");
            sql.append("created_date, ");
            sql.append("updated_date");
            sql.append(") VALUES (");
            sql.append("?, "); 	// login_id
            sql.append("?, "); 	// name
            sql.append("?, "); 	// password
            sql.append("?, "); 	// store
            sql.append("?, "); 	// department
            sql.append("CURRENT_TIMESTAMP, "); // created_date
            sql.append("CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLogin_id());
            ps.setString(2, user.getName());
            ps.setString(3, user.getPassword());
            ps.setInt(4, user.getStore());
            ps.setInt(5, user.getDepartment());
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public void update(Connection connection, User user) {

        PreparedStatement ps = null;
        try {

            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET ");
            sql.append(" login_id = ?");
            sql.append(", name = ?");
            sql.append(", store = ?");
            sql.append(", department = ?");
            sql.append(", updated_date = CURRENT_TIMESTAMP");
            if(StringUtils.isEmpty(user.getPassword()) == false){
            	sql.append(", password = ?");
            }

            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLogin_id());
            ps.setString(2, user.getName());
            ps.setInt(3, user.getStore());
            ps.setInt(4, user.getDepartment());

            if(StringUtils.isEmpty(user.getPassword()) == false){
            	ps.setString(5, user.getPassword());
            	ps.setInt(6, user.getId());
            } else {
            	ps.setInt(5, user.getId());
            }

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }

}