package kadai5.dao;

import static kadai5.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import kadai5.beans.Branch;
import kadai5.exception.SQLRuntimeException;


public class BranchDao {

	public List<Branch> select(Connection connection) {

        PreparedStatement ps = null;

        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("branches.id as id, ");
            sql.append("branches.branches_name as branches_name ");
            sql.append("FROM branches ");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<Branch> ret = toBranchList(rs);
            return ret;

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

	private List<Branch> toBranchList(ResultSet rs)
            throws SQLException {

        List<Branch> ret = new ArrayList<Branch>();
        try {
            while (rs.next()) {
                String branches_name = rs.getString("branches_name");
                int id = rs.getInt("id");

                Branch branch = new Branch();
                branch.setBranches_name(branches_name);
                branch.setId(id);

                ret.add(branch);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
}


