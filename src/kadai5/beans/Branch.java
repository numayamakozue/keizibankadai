package kadai5.beans;

import java.io.Serializable;

public class Branch implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String branches_name;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getBranches_name() {
		return branches_name;
	}
	public void setBranches_name(String branches_name) {
		this.branches_name = branches_name;
	}
}