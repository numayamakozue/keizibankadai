package kadai5.beans;

import java.io.Serializable;
import java.util.Date;

public class UserInfo implements Serializable {
    private static final long serialVersionUID = 1L;
		// TODO 自動生成されたコンストラクター・スタブ
    private int id;
    private int store;
    private int department;
    private String login_id;
    private String password;
    private String name;
    private String branches_name;
    private String divisions_name;
    private Date createdDate;
    private Date updatedDate;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getStore() {
		return store;
	}
	public void setStore(int store) {
		this.store = store;
	}
	public int getDepartment() {
		return department;
	}
	public void setDepartment(int department) {
		this.department = department;
	}
	public String getLogin_id() {
		return login_id;
	}
	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBranches_name() {
		return branches_name;
	}
	public void setBranches_name(String branches_name) {
		this.branches_name = branches_name;
	}
	public String getDivisions_name() {
		return divisions_name;
	}
	public void setDivisions_name(String divisions_name) {
		this.divisions_name = divisions_name;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
}
