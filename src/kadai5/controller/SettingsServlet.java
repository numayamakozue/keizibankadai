package kadai5.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import kadai5.beans.Branch;
import kadai5.beans.Division;
import kadai5.beans.User;
import kadai5.beans.UserInfo;
import kadai5.service.BranchService;
import kadai5.service.DivisionService;
import kadai5.service.UserInfoService;
import kadai5.service.UserService;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

    	int value = Integer.parseInt(request.getParameter("id"));
        UserInfo edit = new UserInfoService().getEdit(value);
        request.setAttribute("edit", edit);

        List<Branch> branch = new BranchService().getBranch();
    	request.setAttribute("branch", branch);

    	List<Division> division = new DivisionService().getDivision();
    	request.setAttribute("division", division);

        request.getRequestDispatcher("/settings.jsp").forward(request, response);
    }
    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

    	List<String> messages = new ArrayList<String>();
        List<Branch> branch = new BranchService().getBranch();
        List<Division> division = new DivisionService().getDivision();
        HttpSession session = request.getSession();
        User editUser = getEditUser(request);

        if (isValid(request, messages) == true) {

        	new UserService().update(editUser);
            response.sendRedirect("./");

        } else {
            session.setAttribute("errorMessages", messages);
            request.setAttribute("branch", branch);
        	request.setAttribute("division", division);
            request.getRequestDispatcher("/settings.jsp").forward(request, response);
        }
    }

    private User getEditUser(HttpServletRequest request)
            throws IOException, ServletException {

        User editUser = new User();
        editUser.setId(Integer.parseInt(request.getParameter("id")));
        editUser.setLogin_id(request.getParameter("login_id"));
        editUser.setName(request.getParameter("name"));
        editUser.setPassword(request.getParameter("password"));
        editUser.setStore(Integer.parseInt(request.getParameter("branch")));
        editUser.setDepartment(Integer.parseInt(request.getParameter("division")));

        return editUser;
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
        String login_id = request.getParameter("login_id");
        String password = request.getParameter("password");
        String password2 = request.getParameter("password2");
        String name = request.getParameter("name");

        UserInfo edit = new UserInfoService().getLogin_id(login_id);

        if (!login_id.matches("^[a-zA-Z0-9]{6,20}$")) {
            messages.add("ログインIDは半角英数字6文字以上20文字以下で入力してください");
        }

        if(edit != null) {
        	messages.add("そのIDはすでに使用されています");
        }

        if (password.isEmpty() == false) {
	         if (!password.matches("^[!-~]{6,20}$")) {
		        messages.add("パスワードは記号を含む半角文字6文字以上20文字以下で入力してください");

		    }else if (!password.equals(password2)) {
		        messages.add("パスワードが一致していません");

	    	}
       }

        if (name.isEmpty() == true) {
        	messages.add("ユーザー名を入力してください");

        }else if (name.length() > 10) {
        	messages.add("ユーザー名は10文字以下で入力してください");
        }

        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}