package kadai5.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import kadai5.beans.Branch;
import kadai5.beans.Division;
import kadai5.beans.User;
import kadai5.service.BranchService;
import kadai5.service.DivisionService;
import kadai5.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	List<Branch> branch = new BranchService().getBranch();
    	request.setAttribute("branch", branch);

    	List<Division> division = new DivisionService().getDivision();
    	request.setAttribute("division", division);

        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<String>();

        HttpSession session = request.getSession();
        if (isValid(request, messages) == true) {

            User user = new User();
            user.setLogin_id(request.getParameter("login_id"));
            user.setPassword(request.getParameter("password"));
            user.setPassword2(request.getParameter("password2"));
            user.setName(request.getParameter("name"));
            user.setStore(Integer.parseInt(request.getParameter("store")));
            user.setDepartment(Integer.parseInt(request.getParameter("department")));

            new UserService().register(user);

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("signup");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
        String login_id = request.getParameter("login_id");
        String password = request.getParameter("password");
        String password2 = request.getParameter("password2");
        String name = request.getParameter("name");

        if (StringUtils.isEmpty(login_id) == true) {
        	messages.add("ログインIDを入力してください");
        }else if(!login_id.matches("^[a-zA-Z0-9]{6,20}$")) {
        	messages.add("ログインIDは半角英数字で6文字以上20文字以下で入力してください");

        }if(!password.matches("^[a-zA-Z0-9]{6,20}$")) {
        	messages.add("パスワードは半角文字で6文字以上20文字以下で入力してください");
        }else if(!password2.equals(password)) {
        	messages.add("パスワードが一致していません");
        }

        if (StringUtils.isEmpty(name) == true) {
            messages.add("名前は10文字以内で入力してください");
        }else if(!name.matches("{10}$")) {
        	messages.add("名前は10文字以内で入力してください");
        }

        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}
