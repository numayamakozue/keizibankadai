package kadai5.service;

import static kadai5.utils.CloseableUtil.*;
import static kadai5.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import kadai5.beans.Branch;
import kadai5.dao.BranchDao;


public class BranchService {

    public List<Branch> getBranch() {

        Connection connection = null;
        try {
            connection = getConnection();

            BranchDao branchDao = new BranchDao();
            List<Branch> ret = branchDao.select(connection);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}