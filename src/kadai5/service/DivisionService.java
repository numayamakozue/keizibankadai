package kadai5.service;

import static kadai5.utils.CloseableUtil.*;
import static kadai5.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import kadai5.beans.Division;
import kadai5.dao.DivisionDao;


public class DivisionService{

    public List<Division> getDivision() {

        Connection connection = null;
        try {
            connection = getConnection();

            DivisionDao divisionDao = new DivisionDao();
            List<Division> ret = divisionDao.select(connection);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}
