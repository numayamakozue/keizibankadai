package kadai5.service;

import static kadai5.utils.CloseableUtil.*;
import static kadai5.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import kadai5.beans.UserInfo;
import kadai5.dao.UserInfoDao;

public class UserInfoService {

    public List<UserInfo> getUserInfo() {

        Connection connection = null;

        try {
            connection = getConnection();

            UserInfoDao userinfoDao = new UserInfoDao();
            List<UserInfo> ret = userinfoDao.getUserInfo(connection);

            commit(connection);

            return ret;

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;

        } catch (Error e) {
            rollback(connection);
            throw e;

        } finally {
            close(connection);
        }
    }

    public UserInfo getEdit(int value) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserInfoDao userinfoDao = new UserInfoDao();
            UserInfo user = userinfoDao.getEdit(connection, value);

            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public UserInfo getLogin_id(String value) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserInfoDao userinfoDao = new UserInfoDao();
            UserInfo user = userinfoDao.getLogin_id(connection, value);

            commit(connection);

            return user;

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;

        } catch (Error e) {
            rollback(connection);
            throw e;

        } finally {
            close(connection);
        }
    }

}